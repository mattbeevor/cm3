const fs = require('fs');
let arr = require('../dataIn.json')
const assetsPath="./assets/"
const staticPath="./static/"
const output="dataOut.json"
const { Image } = require('image-js');
var probe = require('probe-image-size');

console.log("load images")


let promises = arr.map(x=>probe(fs.createReadStream('./static/'+x.src)))

Promise.all(promises).then((values)=>{
	console.log("all loaded")
	values.forEach((im,i)=>{
	  let x = arr[i]
	  x.ratio = im.width/im.height
	  console.log(`${i}/${arr.length}`,)
	})
  fs.writeFileSync(assetsPath+output, JSON.stringify(arr));
  fs.writeFileSync(staticPath+output, JSON.stringify(arr));
  console.log("wrote file")
});